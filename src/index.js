import './sass/main.scss';

window.jQuery = window.$ = require('jquery');

import 'bootstrap';
import 'popper.js';
import '@fortawesome/fontawesome-free';
import 'aos';

import './js/module-1';
import './js/module-2';
import './js/module-3';