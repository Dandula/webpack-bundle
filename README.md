# Webpack Bundle

## Доступные команды

Запуск Webpack Dev Server
```
npm run start:dev
```

Запуск Development-сборки в режиме наблюдения
```
npm run build:dev
```

Запуск Production-сборки
```
npm run build:prod
```